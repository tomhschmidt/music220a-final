var Forecast = require('forecast.io');
var forecastOptions = {
	APIKey: 'a3f6f3c13f3c3754597f546d4849d5f8',
	requestTimeout: 1000
};

var forecast = new Forecast(forecastOptions);

var Twitter = require('./node_modules/TwitterJSClient/lib/Twitter.js').Twitter;
var twitterOptions = {
	"consumerKey" : "POYGUaNMcAl5nXJvj50sUA",
	"consumerSecret" : "VdiAutujTU6RNokjsM6ZNkzidUoFQh5rTaOGUg",
	"accessToken" : "239090734-bkgSImZnSI4ZEUPKquMmtit4GYUB3uhboWEyMH12",
	"accessTokenSecret" : "9MmzkPM8Rgnv7s8MH33SgIM7LyVxxB7iwPlEyBmF4abss",
}

var BEIJING_LAT = 39.9139;
var BEIJING_LONG = 116.3917;

var TWITTER_REFRESH_INTERVAL = 15;
var FORECAST_REFRESH_INTERVAL = 15;

var osc = require('node-osc');

var client = new osc.Client('localhost', 3333);

var lastTweetID = 0;
var twitter = new Twitter(twitterOptions);

var twitterError = function (err, response, body) {
    console.log('ERROR [%s]', err);
};
var twitterSuccess = function (data) {
	data = JSON.parse(data);
	curID = parseInt(data[0]['id']);
    if (lastTweetID < curID) {
    	lastTweetID = curID;
    	console.log("Newest tweet ID is " + lastTweetID);
    	var text = data[0]['text'];
    	var tokens = text.split("; ");
    	if(tokens.length >= 4) {
	    	var newAQI = parseInt(tokens[3]);
	    	console.log("Sending AQI value " + newAQI);
	    	client.send('/twitter', newAQI);
    	} else {
    		console.log("Malformed tweet. Could not parse AQI data");
    	}
    }
};

var lastWeatherTimestamp = 0;
var weatherResponse = function (err, res, data) {
	if (err) {
		console.log(err);
	} else {
		//console.log(data);
		curData = data['hourly']['data'][0];
		var curTime = parseInt(curData['time']);
		if (parseInt(curData['time']) > lastWeatherTimestamp) {
			console.log("Received a new weather reading for time " + curTime);
			lastWeatherTimestamp = curTime;

			var windSpeed = parseFloat(curData['windSpeed']);
			var windDirection = parseInt(curData['windBearing']);
			console.log("Sending wind speed " + windSpeed + "\t wind direction " + windDirection);
			client.send('/wind', windSpeed, windDirection);
		}
	}
}

setInterval(function() {
	console.log("Polling Twitter");
	twitter.getUserTimeline({ screen_name: 'BeijingAir', count : '1'}, twitterError, twitterSuccess);
}, TWITTER_REFRESH_INTERVAL * 1000);

setInterval(function() {
	console.log("Polling Forecast.io");
	forecast.get(BEIJING_LAT, BEIJING_LONG, weatherResponse);
}, FORECAST_REFRESH_INTERVAL * 1000);
