var osc = require('node-osc');

var client = new osc.Client('localhost', 3333);

var newAQI = 0;
var windSpeed = 40.2;
var windDirection = 300;

console.log("sending wind");
client.send('/wind', windSpeed, windDirection);
console.log("sent wind");

client.send('/twitter', newAQI);