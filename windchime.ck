// (launch with s.ck)

// create our OSC receiver
OscRecv recv;
// use port 6449 (or whatever)
3333 => recv.port;
// start listening (launch thread)
recv.listen();

// create an address in the receiver, store in new variable
recv.event( "/twitter, i" ) @=> OscEvent @ twitter;
recv.event( "/wind, f i" ) @=> OscEvent @ wind;

0 => int aqi;
0 => int direction;
5 => float speed;

8 => int numBells;
TubeBell bells[numBells];

LPF filters[numBells];
1300 => float freqCap;

[220.0, 293.7, 349.2, 392.0, 440.0, 587.3, 698.2, 880.0] @=> float freqs[];

bells[0].freq(220); // a3
bells[1].freq(293.7); // d4
bells[2].freq(349.2); // f4
bells[3].freq(392); // g4
bells[4].freq(440); // a4
bells[5].freq(587.3); // d5
bells[6].freq(698.2); // f5
bells[7].freq(880); // a5

for(0 => int i; i < numBells; i++) {
    bells[i].freq(freqs[i]);
    <<< "freq : ", (freqCap - (aqi * 2.4)) >>>;
    filters[i].freq(1300);//freqCap - (aqi * 4.2));
    if(i % 2 == 0) {
        bells[i] => filters[i] => dac.left;
    } else {
        bells[i] => filters[i] => dac.right;
    }
}


0 => float rnd;
.008 => float changePercent;
.002 => float noise;
0 => float diff;
0 => float changeAmount;
100 => float newValue;
100 => float oldValue;
1 => int trendUp;
0.95 => float probTrendSwitch;

0 => int lowerRandomBound;
1 => int upperRandomBound;

fun float getNewValue() {
    Math.random2f(lowerRandomBound, upperRandomBound) * newValue * changePercent => diff;
    if(trendUp == 1) {
        diff + newValue => newValue;
    } else {
        -1 * diff + newValue => newValue;
    }
    
    if(Math.random2f(lowerRandomBound, upperRandomBound) > .5) {
        newValue + (newValue * Math.random2f(lowerRandomBound, upperRandomBound) * noise) => newValue;
    } else {
        newValue + (newValue * Math.random2f(lowerRandomBound, upperRandomBound) * -1 * noise) => newValue;
    }
    
    if(Math.random2f(lowerRandomBound, upperRandomBound) > probTrendSwitch) {
        if(trendUp == 1) {
            0 => trendUp;
        } else {
            1 => trendUp;
        }
    }
    
    if (newValue > 130) {
        130 => newValue;
    } else if(newValue < 70) {
        70 => newValue;
    }
    
    return newValue;
}

.01 => float yDiff;
.05 => float mu;
.2 => float sigma;
100 => float initValue;

fun float brownianNextValue() {
   Math.sqrt(yDiff) => float sqrtYDiff;
   sqrtYDiff * Math.random2f(0, 100) => float deltaW;
   (mu - (sigma * sigma / 2)) * yDiff + sigma * deltaW => float increments;
   Math.exp(increments) => float expIncrements;
   return initValue * expIncrements;
}

.1 => float bellVelMinConst;
.3 => float bellVelMaxConst;
bellVelMinConst => float bellVelMin;
bellVelMaxConst => float bellVelMax;

40 => int maxWindSpeed;

100 => float curValue;

// infinite event loop
while( true )
{
    // wait for event to arrive
    //twitter => now;

    // grab the next message from the queue. 
    while( twitter.nextMsg() )
    {
        twitter.getInt() => aqi;
        
        freqCap - (aqi * 2.4) => float newFreqCap;
        if(newFreqCap < 50) {
            50 => newFreqCap;
        }
        for(0 => int i; i < numBells; i++) {
            filters[i].freq(newFreqCap);
        }
        // print
        <<< "got new AQI Rating:", aqi >>>;
    }
    
    //wind => now;
    
   4 - (3 * speed / maxWindSpeed) - ((curValue - 100) / 5)  => float timeDelayMax;
   
   if(timeDelayMax < 0) {
       .3 => timeDelayMax;
   }
    
    Math.random2f(0, timeDelayMax)::second => now;
    
    while( wind.nextMsg() )
    {
        wind.getFloat() => speed;
        wind.getInt() => direction;
        
        <<< "got new wind readings:", speed, direction >>>;
        
        bellVelMinConst + .7 * (speed / maxWindSpeed) => bellVelMin;
        bellVelMaxConst + .7 * (speed / maxWindSpeed) => bellVelMax;
        
    }
    
    Math.random2(0, numBells - 1) => int bell;
    
    Math.random2f(bellVelMin, bellVelMax) => bells[bell].noteOn;
    //0 => bells[bell].noteOff;

    
   // Math.random2f(0, 1) => rnd;
   // 2 * volatility * rnd => changePercent;
   // if (changePercent > volatility) {
   //     changePercent - (2 * volatility) => changePercent;
   // }
    
    //oldValue * changePercent => changeAmount;
    //oldValue + changeAmount => newValue;
    
    
    getNewValue() => curValue;

   
   <<< "newValue : ", curValue >>>;
}
